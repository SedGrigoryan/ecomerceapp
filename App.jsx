/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import AppNavigator from './src/Screens';
import {Provider as StoreProvider} from 'react-redux';
import store from './src/Store';
function App() {
  return (
    <StoreProvider store={store}>
      <AppNavigator />
    </StoreProvider>
  );
}

export default App;
