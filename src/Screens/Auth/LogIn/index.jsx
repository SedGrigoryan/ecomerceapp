import React from 'react';
import {
  View,
  Text,
  Image,
  Platform,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
} from 'react-native';
import LoginForm from './LoginForm';
import logo from '../../../Assets/images/logo/logo.png';

function LogIn() {
  return (
    <KeyboardAvoidingView
      style={styles.keyboardView}
      behavior={'padding'}
      keyboardVerticalOffset={Platform.OS === 'ios' ? 0 : 0}
    >
      <ScrollView
        contentContainerStyle={styles.view}
        keyboardDismissMode={'interactive'}>
        <View style={styles.logoContainer}>
          <Text style={styles.title}>LOG IN</Text>
          <Image source={logo} style={styles.logo} />
        </View>
        <LoginForm />
      </ScrollView>
    </KeyboardAvoidingView>
  );
}

export default LogIn;

const styles = StyleSheet.create({
  keyboardView: {
    flex: 1,
  },
  view: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 24,
    fontFamily: Platform.OS === 'ios' ? 'Gotham-Black' : 'Gotham-Medium',
    fontWeight: '500',
    textAlign: 'center',
    marginBottom: 45,
    color: '#1E1D1D',
  },
  logoContainer: {
    flex: 0.4,
    justifyContent: 'center',
  },
  logo: {
    width: 140,
    height: 84,
  },
});
