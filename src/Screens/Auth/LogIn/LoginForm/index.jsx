import React, {useRef, useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {Formik} from 'formik';
import DefaultInput from '../../../../Components/DefaultInput/DefaultInput.jsx';
import {loginAction} from '../../../../Store/reducers/authSlice';
import {validationSchema} from './validationSchema.js';

function LoginForm() {
  const formikRef = useRef();
  const dispatch = useDispatch();
  const {loading, loginError} = useSelector(state => state.authReducer);

  const handleSubmit = useCallback(() => {
    formikRef?.current?.handleSubmit();
  }, []);

  return (
    <Formik
      innerRef={formikRef}
      initialValues={{
        username: 'atuny0',
        password: '9uQFF1Lh',
      }}
      onSubmit={async values => {
        dispatch(loginAction(values));
      }}
      validationSchema={validationSchema}>
      {formikProps => (
        <View style={styles.formContainer}>
          <DefaultInput
            label="Username"
            name="username"
            formikProps={formikProps}
          />
          <DefaultInput
            label="Password"
            name="password"
            formikProps={formikProps}
          />
          {loginError ? (
            <View style={styles.errorBox}>
              <Text style={styles.error}>{loginError}</Text>
            </View>
          ) : null}

          <View style={styles.buttonContainer}>
            <TouchableOpacity
              style={styles.button}
              activeOpacity={0.8}
              onPress={handleSubmit}>
              {loading ? (
                <ActivityIndicator color={'#fff'} size={'small'} />
              ) : (
                <Text style={styles.buttonText}>Login</Text>
              )}
            </TouchableOpacity>
          </View>
        </View>
      )}
    </Formik>
  );
}

export default LoginForm;

const styles = StyleSheet.create({
  formContainer: {
    paddingHorizontal: 15,
    flex: 0.5,
  },
  buttonContainer: {
    flex: 1,
    marginHorizontal: 5,
    justifyContent: 'flex-end',
    marginBottom: 25,
  },
  button: {
    height: 44,
    borderRadius: 10,
    backgroundColor: '#7867BE',
    alignItems: 'center',
    justifyContent: 'center',
  },

  buttonText: {
    fontSize: 14,
    textTransform: 'uppercase',
    textAlign: 'center',
    color: '#fff',
    fontFamily: 'Gotham-Book',
  },
  errorBox: {
    paddingHorizontal: 15,
  },
  error: {
    color: '#ee2864',
    fontSize: 14,
    fontWeight: '500',
    fontFamily: 'Gotham-Book',
  },
});
