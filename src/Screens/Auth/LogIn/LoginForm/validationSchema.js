import * as yup from 'yup';

export const validationSchema = () => {
  return yup.object().shape({
    username: yup
      .string()
      .required('Username is required')
      .min(6, 'Seems a bit short...'),
    password: yup
      .string()
      .required('Password is required')
      .min(6, 'Seems a bit short...')
      .max(15, 'Password must be not large than 15 symbols'),
  });
};
