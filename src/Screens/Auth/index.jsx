import React from 'react';
import {StyleSheet, SafeAreaView} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import LogIn from './LogIn';

const Stack = createStackNavigator();

function AuthRoutes() {
  return (
    <SafeAreaView style={styles.view}>
      <Stack.Navigator
        initialRouteName={'LogIn'}
        screenOptions={{headerShown: false}}>
        <Stack.Screen name={'LogIn'} component={LogIn} />
      </Stack.Navigator>
    </SafeAreaView>
  );
}

export default AuthRoutes;

const styles = StyleSheet.create({
  view: {
    flex: 1,
  },
});
