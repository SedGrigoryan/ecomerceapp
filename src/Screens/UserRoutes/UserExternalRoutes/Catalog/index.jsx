import React, {useEffect, useState} from 'react';
import {StyleSheet, ScrollView, View} from 'react-native';
import {FlatGrid} from 'react-native-super-grid';
import StackScreenHeader from '../../../../Components/StackScreenHeader';
import ProductItem from '../../../../Components/ProductItem';
import SkeletonPlaceholders from '../../../../Components/SkeletonPlaceholder';
import {getProductsByCategory} from '../../../../Actions/Products';
function Catalog({route}) {
  const [products, setProducts] = useState([]);
  const {categoryName} = route.params;

  useEffect(() => {
    (async function getProducts() {
      if (categoryName) {
        const productsData = await getProductsByCategory(categoryName);
        setProducts(productsData);
      }
    })();
  }, [categoryName]);

  return (
    <View style={styles.root}>
      <StackScreenHeader
        leftIcon={'chevron-back-outline'}
        routeName={categoryName?.toUpperCase() || ''}
        rightIcon={'search-sharp'}
      />
      {products ? (
        products.length ? (
          <FlatGrid
            itemDimension={150}
            data={products}
            renderItem={({item, index}) => (
              <ProductItem key={index} item={item} />
            )}
          />
        ) : (
          <FlatGrid
            data={['1', '2', '3', '4', '5', '6']}
            itemDimension={150}
            renderItem={({item}) => (
              <SkeletonPlaceholders key={`skeleton-${item}`} type={'product'} />
            )}
          />
        )
      ) : null}
    </View>
  );
}

export default Catalog;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
