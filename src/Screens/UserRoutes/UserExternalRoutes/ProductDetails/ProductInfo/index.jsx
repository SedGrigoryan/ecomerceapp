import React, {useCallback} from 'react';
import {
  View,
  Text,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons';

function ProductInfo({product}) {
  const countDisCountedPrice = useCallback(() => {
    if (product?.price && product?.discountPercentage) {
      return Math.round(
        product.price - (product.price * product.discountPercentage) / 100,
      );
    }
  }, [product]);

  return (
    <View style={styles.container}>
      <Text style={styles.productTitle}>{product?.title || ''}</Text>

      <View style={styles.rowBoxWithTopMargin}>
        <Text style={styles.price}>{product?.price || ''}</Text>
        <Text style={[styles.discountedPrice, styles.leftSpace]}>
          {countDisCountedPrice(product) || ''}$
        </Text>
      </View>
      <View style={styles.rowBoxWithTopMargin}>
        <Text style={styles.strongText}>Rating</Text>
        <View style={[styles.rowBox, styles.leftSpace]}>
          <Ionicons color={'#FFC700'} name={'star'} size={14} />
          <Text style={styles.lightText}>{product?.rating || ''}</Text>
        </View>
      </View>
      <View style={styles.rowBoxWithTopMargin}>
        <Text style={styles.strongText}>ID: </Text>
        <Text style={[styles.lightText, styles.leftSpace]}>
          {product?.id || ''}
        </Text>
      </View>
      <View style={styles.rowBoxWithTopMargin}>
        <Text style={styles.strongText}>Brand</Text>

        <Text style={[styles.lightText, styles.leftSpace]}>
          {product?.brand || ''}
        </Text>
      </View>
      <View style={styles.rowBoxWithTopMargin}>
        <Text style={styles.strongText}>Category</Text>

        <Text style={[styles.lightText, styles.leftSpace]}>
          {product?.category?.slice(0, 1).toUpperCase() +
            product?.category?.slice(1) || ''}
        </Text>
      </View>
      <View style={styles.descriptionBox}>
        <Text style={styles.description}>{product?.description || ''}</Text>
      </View>
      <View style={styles.totalBox}>
        <View styles={styles.totalPriceBox}>
          <Text style={styles.lightText}>Total</Text>
          <Text style={styles.totalPrice}>
            {countDisCountedPrice(product) || ''}$
          </Text>
        </View>
        <TouchableOpacity style={styles.addCardButton} activeOpacity={0.8}>
          <Text style={styles.addCardButtonText}>Add to card</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default ProductInfo;

const styles = StyleSheet.create({
  container: {
    marginTop: 25,
    paddingHorizontal: 25,
  },

  productTitle: {
    fontSize: 16,
    fontWeight: '500',
    textTransform: 'uppercase',
    fontFamily: 'Gotham-Book',
  },

  rowBox: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  price: {
    fontSize: 14,
    color: '#F34040',
    textDecorationLine: 'line-through',
    fontFamily: 'Gotham',
    fontWeight: '300',
  },
  discountedPrice: {
    fontSize: 16,
    color: '#1E1D1D',
    fontWeight: '500',
    fontFamily: 'Gotham',
    paddingLeft: 10,
  },

  leftSpace: {
    paddingLeft: 15,
  },

  rowBoxWithTopMargin: {
    flexDirection: 'row',
    alignItems: 'center',
    marginTop: 15,
  },
  strongText: {
    fontSize: 14,
    color: '#1E1D1D',
    fontWeight: '400',
    fontFamily: 'Gotham',
  },
  lightText: {
    fontSize: 14,
    color: '#1E1D1D',
    fontFamily: 'Gotham',
    fontWeight: '300',
  },
  descriptionBox: {
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#E6E6E6',
    marginVertical: 20,
  },
  description: {
    fontSize: 14,
    fontWeight: '300',
    fontFamily: 'Gotham',
    color: '#8F8F8F',
    paddingVertical: 20,
    paddingHorizontal: 5,
    textAlign: 'justify',
    lineHeight: 30,
  },
  totalBox: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 15,
    marginTop: 15,
  },
  totalPriceBox: {
    borderWidth: 1,
  },
  totalPrice: {
    marginTop: 10,
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Gotham',
    color: '#1D1E1D',
  },
  addCardButton: {
    backgroundColor: '#7867BE',
    paddingVertical: 18,
    paddingHorizontal: 35,
    borderRadius: 10,
  },

  addCardButtonText: {
    color: '#fff',
    textTransform: 'uppercase',
    fontWeight: '400',
    fontFamily: 'Gotham',
    textAlign: 'center',
  },
});
