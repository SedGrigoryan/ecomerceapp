import React, {useState, useEffect, useMemo} from 'react';
import {View, Text, StyleSheet, ScrollView, Dimensions} from 'react-native';
import {SliderBox} from 'react-native-image-slider-box';
import SkeletonPlaceholders from '../../../../../Components/SkeletonPlaceholder';
const {width} = Dimensions.get('window');

function ProductBanner({productDetails}) {

  return (
    <>
      {productDetails ? (
        productDetails.images?.length ? (
          <SliderBox
            dotColor="#000"
            inactiveDotColor="#fff"
            images={productDetails.images}
            sliderBoxHeight={343}
            autoplay={true}
            autoplayInterval={5000}
            ImageComponentStyle={{
              borderRadius: 10,
              width: width * 0.93,
            }}
          />
        ) : null
      ) : (
        <SkeletonPlaceholders type={'product'} height={343} width={1} />
      )}
    </>
  );
}

export default ProductBanner;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
