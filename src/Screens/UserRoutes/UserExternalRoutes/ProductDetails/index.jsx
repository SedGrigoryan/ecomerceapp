import React, {useState, useEffect} from 'react';
import {StyleSheet, ScrollView} from 'react-native';
import StackScreenHeader from '../../../../Components/StackScreenHeader';
import ProductInfo from './ProductInfo';
import ProductBanner from './ProductBanner';
import {getProductDetails} from '../../../../Actions/Products';

function ProductDetails({route}) {
  const {productId} = route.params;
  const [productDetails, setProductDetils] = useState();

  useEffect(() => {
    (async function getDetails() {
      if (productId) {
        const product = await getProductDetails(productId);
        setProductDetils(product);
      }
    })();
  }, [productId]);

  return (
    <ScrollView
      style={styles.root}
      contentContainerStyle={styles.contentContainer}>
      <StackScreenHeader
        leftIcon={'chevron-back-outline'}
        rightIcon={'heart-outline'}
        rightIconAction="favorite"
        search={false}
        product={productDetails}
      />
      <ProductBanner productDetails={productDetails} />
      <ProductInfo product={productDetails} />
    </ScrollView>
  );
}

export default ProductDetails;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
