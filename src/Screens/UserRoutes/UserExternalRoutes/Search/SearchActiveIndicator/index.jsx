import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';

function SearchActiveIndicator({isLoading}) {
  return (
    <>
      {isLoading ? (
        <View style={styles.container}>
          <View style={styles.indicator}>
            <ActivityIndicator size="medium" color="#7867BE" />
          </View>
        </View>
      ) : null}
    </>
  );
}

export default SearchActiveIndicator;

const styles = StyleSheet.create({
  container: {
    paddingVertical: 20,
  },
  indicator: {
    alignItems: 'center',
    backgroundColor: 'white',
  },
});
