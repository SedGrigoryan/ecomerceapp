import React, {useCallback} from 'react';
import {View, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {FlatGrid} from 'react-native-super-grid';
import SearchHeader from './SearchHeader';
import ProductItem from '../../../../Components/ProductItem';
import SkeletonPlaceholders from '../../../../Components/SkeletonPlaceholder';
import SearchActiveIndicator from './SearchActiveIndicator';
import {searchAction} from '../../../../Store/reducers/searchSlice';
function Search() {
  const dispatch = useDispatch();
  const {searchResult, loading, total, skip, limit, searchText} = useSelector(
    state => state.searchReducer,
  );

console.log('loading: ', loading)
  const onEndReach = useCallback(() => {
    if (searchResult?.length < total) {
      dispatch(searchAction({total, skip, limit, value: searchText}));
    }
  }, [dispatch, limit, searchResult.length, searchText, skip, total]);

  return (
    <View style={styles.root}>
      <SearchHeader />
      <View style={styles.root}>
        {searchResult ? (
          searchResult?.length ? (
            <FlatGrid
              itemDimension={150}
              data={searchResult}
              renderItem={({item, index}) => (
                <ProductItem key={index} item={item} />
              )}
              initialNumToRender={10}
              onEndReached={onEndReach}
              onEndReachedThreshold={0.5}
              keyExtractor={item => item.title}
              ListFooterComponent={<SearchActiveIndicator isLoading={loading} />}
            />
          ) : null
        ) : (
          <FlatGrid
            data={['1', '2', '3', '4', '5', '6']}
            itemDimension={150}
            renderItem={({item}) => (
              <SkeletonPlaceholders key={`skeleton-${item}`} type={'product'} />
            )}
          />
        )}
      </View>
    </View>
  );
}

export default Search;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
    paddingBottom: 15,
  },
  contentContainer: {
    paddingVertical: 15,
    alignItems: 'center',
    justifyContent: 'center',
    // borderWidth: 1,
  },
});
