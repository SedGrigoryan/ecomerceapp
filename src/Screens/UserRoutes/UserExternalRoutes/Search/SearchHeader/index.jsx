import React, {useCallback, useState} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchInput from '../SearchInput';
import {clearAction} from '../../../../../Store/reducers/searchSlice';

function SearchHeader() {
  const [active, setActive] = useState(false);
  const navigation = useNavigation();
  const dispatch = useDispatch();

  const onBackButtonClick = useCallback(() => {
    dispatch(clearAction());
    navigation.goBack();
  }, [dispatch, navigation]);

  const onActiveValue = useCallback(value => {
    setActive(value);
  }, []);

  return (
    <View style={styles.root}>
      {!active ? (
        <TouchableOpacity onPress={onBackButtonClick}>
          <Ionicons name={'chevron-back-outline'} color={'#343434'} size={25} />
        </TouchableOpacity>
      ) : null}
      <SearchInput
        active={active}
        onActiveValue={value => onActiveValue(value)}
      />
    </View>
  );
}

export default SearchHeader;

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: '#fff',
  },
  logo: {
    width: 43,
    height: 26,
  },
  routeName: {
    fontSize: 15,
    fontWeight: '500',
    textTransform: 'uppercase',
  },
  emptyView: {
    paddingVertical: 12,
  },
});
