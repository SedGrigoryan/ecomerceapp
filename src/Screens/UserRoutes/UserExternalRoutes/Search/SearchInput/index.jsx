import React, {useState, useCallback, useRef} from 'react';
import {TextInput, StyleSheet, TouchableOpacity} from 'react-native';
import {useDispatch} from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';
import {initialSearchAction} from '../../../../../Store/reducers/searchSlice';
import {setSearchText} from '../../../../../Store/reducers/searchSlice';

function SearchInput({onActiveValue, active}) {
  const [text, setText] = useState('');
  const dispatch = useDispatch();
  const inputRef = useRef();

  const onTextChange = useCallback(
    value => {
      setText(value);
      dispatch(setSearchText(value));
      dispatch(initialSearchAction(value));
    },
    [dispatch],
  );

  const onRemoveText = useCallback(() => {
    setText('');
    dispatch(setSearchText(''));
    dispatch(initialSearchAction(''));
    onActiveValue(false);
    inputRef.current.blur();
  }, [dispatch]);

  const onTextInputPressIn = useCallback(() => {
    onActiveValue(true);
  }, [onActiveValue]);

  const onTextInputPressOut = useCallback(() => {
    onActiveValue(false);
  }, [onActiveValue]);

  return (
    <TouchableOpacity style={styles.root} onPress={onTextInputPressIn}>
      <Icon name="search-sharp" color="#9D9D9D" size={20} />
      <TextInput
        ref={inputRef}
        value={text}
        style={styles.textInput}
        placeholder={!active ? 'Saerch' : null}
        onFocus={onTextInputPressIn}
        onBlur={onTextInputPressOut}
        onChangeText={onTextChange}
        placeholderTextColor={'#1E1D1D'}
      />

      {active ? (
        <TouchableOpacity onPress={onRemoveText}>
          <Icon name="close" color="#9D9D9D" size={20} />
        </TouchableOpacity>
      ) : null}
    </TouchableOpacity>
  );
}

export default SearchInput;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    height: 40,
    paddingHorizontal: 15,
    backgroundColor: '#F4F4F4',
    borderRadius: 5,
    marginLeft: 15,
  },
  textInput: {
    paddingLeft: 10,
    flex: 1,
    marginHorizontal: 5,
    height: '100%',
    color: '#1E1D1D',
  },
});
