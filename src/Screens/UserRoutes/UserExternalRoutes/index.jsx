import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import Search from './Search';
import Catalog from './Catalog';
import ProductDetails from './ProductDetails';

const Stack = createStackNavigator();

function UserExternalRoutes() {
  return (
    <SafeAreaView style={styles.root}>
      <Stack.Navigator
        initialRouteName={'UserExternalRoutes'}
        screenOptions={{
          headerMode: 'none',
        }}>
        <Stack.Screen name={'Search'} component={Search} />
        <Stack.Screen name={'Catalog'} component={Catalog} />
        <Stack.Screen name={'ProductDetails'} component={ProductDetails} />
      </Stack.Navigator>
    </SafeAreaView>
  );
}

export default UserExternalRoutes;

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});
