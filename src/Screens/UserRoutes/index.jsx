import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {createStackNavigator} from '@react-navigation/stack';
import UserTabRoutes from './UserTabRoutes';
import UserExternalRoutes from './UserExternalRoutes';

const Stack = createStackNavigator();

function UserRoutes() {
  return (
    <SafeAreaView style={styles.root}>
      <Stack.Navigator
        initialRouteName="UserTabRoutes"
        screenOptions={{
          headerMode: 'none',
        }}>
        <Stack.Screen name={'UserTabRoutes'} component={UserTabRoutes} />
        <Stack.Screen
          name={'UserExternalRoutes'}
          component={UserExternalRoutes}
        />
      </Stack.Navigator>
    </SafeAreaView>
  );
}

export default UserRoutes;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
});
