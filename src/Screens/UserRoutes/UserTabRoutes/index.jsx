import React from 'react';
import {StyleSheet} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from './Home';
import Categories from './Categories';
import Wishlist from './WishList';
import Profile from './Profile';
import Octicons from 'react-native-vector-icons/Octicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import TabScreenHeader from '../../../Components/TabScreenHeader';
import {SafeAreaView} from 'react-native';

const Tab = createBottomTabNavigator();

function UserTabRoutes() {
  return (
    <SafeAreaView style={styles.root}>
      <Tab.Navigator
        initialRouteName="Home"
        activeColor="#7867BE"
        inactiveColor="#CACACA"
        backBehavior="history"
        screenOptions={{
          tabBarActiveTintColor: '#7867BE',
          tabBarInactiveTintColor: '#CACACA',
        }}>
        <Tab.Screen
          name={'Home'}
          component={Home}
          options={{
            headerShown: true,
            header: () => (
              <TabScreenHeader img={true} rightIcon={'search-sharp'} />
            ),
            title: 'Home',
            tabBarIcon: ({color}) => (
              <Octicons name="home" color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name={'Categories'}
          component={Categories}
          options={{
            headerShown: true,
            header: () => (
              <TabScreenHeader
                routeName={'Categories'}
                leftIcon={'chevron-back-outline'}
                rightIcon={'search-sharp'}
              />
            ),
            title: 'Categories',
            tabBarIcon: ({color}) => (
              <AntDesign name="appstore-o" color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name={'Wishlist'}
          component={Wishlist}
          options={{
            headerShown: true,
            header: () => (
              <TabScreenHeader
                routeName={'Wishlist'}
                leftIcon={''}
                rightIcon={'search-sharp'}
              />
            ),
            title: 'WishList',
            tabBarIcon: ({color}) => (
              <AntDesign name="hearto" color={color} size={20} />
            ),
          }}
        />
        <Tab.Screen
          name={'Profile'}
          component={Profile}
          options={{
            headerShown: true,
            header: () => <TabScreenHeader routeName={'Profile'} />,
            title: 'Profile',
            tabBarIcon: ({color}) => (
              <MaterialIcons name="person-outline" color={color} size={20} />
            ),
          }}
        />
      </Tab.Navigator>
    </SafeAreaView>
  );
}

export default UserTabRoutes;

const styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});
