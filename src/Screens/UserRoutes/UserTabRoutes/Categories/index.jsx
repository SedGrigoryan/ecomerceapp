import React, {useMemo} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {useSelector} from 'react-redux';
import {FlatGrid} from 'react-native-super-grid';
import SkeletonPlaceholders from '../../../../Components/SkeletonPlaceholder';
import CategoryItem from './CategoryItem';
import {categoriesImageMapData} from './data';

function Categories() {
  const {categories} = useSelector(state => state.categoryReducer);

  const categoriesData = useMemo(() => {
    const categoresWithPictures = [];
    if (categories?.length) {
      categories.forEach(item => {
        categoresWithPictures.push({
          name: item,
          img: categoriesImageMapData[item],
        });
      });
    }
    return categoresWithPictures;
  }, [categories]);

  return (
    <View style={styles.root}>
      {categoriesData?.length ? (
        <FlatList
          data={categoriesData}
          keyExtractor={item => item.name}
          renderItem={({item}) => <CategoryItem key={item.name} item={item} />}
        />
      ) : (
        <FlatGrid
          data={['1', '2', '3', '4', '5', '6']}
          itemDimension={500}
          keyExtractor={item => item}
          renderItem={({item}) => (
            <SkeletonPlaceholders
              key={`skeleton-${item}`}
              type={'product'}
              height={125}
              width={1}
            />
          )}
        />
      )}
    </View>
  );
}

export default Categories;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#F9F9F9',
    paddingVertical: 20,
    paddingHorizontal: 15,
  },
});
