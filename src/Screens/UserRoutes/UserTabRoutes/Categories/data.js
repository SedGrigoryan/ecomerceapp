import fragrances from '../../../../Assets/images/categories/fragrances.jpg';
import automotive from '../../../../Assets/images/categories/automotive.jpg';
import furniture from '../../../../Assets/images/categories/furniture.jpg';
import groceries from '../../../../Assets/images/categories/groceries.jpg';
import tops from '../../../../Assets/images/categories/tops.jpg';
import homeDecoration from '../../../../Assets/images/categories/home-decoration.jpg';
import laptops from '../../../../Assets/images/categories/laptops.jpg';
import lighting from '../../../../Assets/images/categories/lighting.jpg';
import mensShirts from '../../../../Assets/images/categories/mens-shirts.jpg';
import mensShoes from '../../../../Assets/images/categories/mens-shoes.jpg';
import mensWatches from '../../../../Assets/images/categories/mens-watches.jpg';
import motorcycle from '../../../../Assets/images/categories/motorcycle.jpg';
import skincare from '../../../../Assets/images/categories/skincare.jpg';
import smartphones from '../../../../Assets/images/categories/smartphones.jpg';
import sunglasses from '../../../../Assets/images/categories/sunglasses.jpg';
import womensBags from '../../../../Assets/images/categories/womens-bags.jpg';
import womensDresses from '../../../../Assets/images/categories/womens-dresses.jpg';
import womensJewellery from '../../../../Assets/images/categories/womens-jewellery.jpg';
import womensShoes from '../../../../Assets/images/categories/womens-shoes.jpg';
import womensWatches from '../../../../Assets/images/categories/womens-watches.jpg';

export const categoriesImageMapData = {
  smartphones: smartphones,
  laptops: laptops,
  fragrances: fragrances,
  skincare: skincare,
  groceries: groceries,
  'home-decoration': homeDecoration,
  furniture: furniture,
  tops: tops,
  'womens-dresses': womensDresses,
  'womens-shoes': womensShoes,
  'mens-shirts': mensShirts,
  'mens-shoes': mensShoes,
  'mens-watches': mensWatches,
  'womens-watches': womensWatches,
  'womens-bags': womensBags,
  'womens-jewellery': womensJewellery,
  sunglasses: sunglasses,
  automotive: automotive,
  motorcycle: motorcycle,
  lighting: lighting,
};
