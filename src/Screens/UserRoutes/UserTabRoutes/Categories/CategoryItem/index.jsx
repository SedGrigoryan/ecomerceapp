import React, {useCallback} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Platform,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import AutoHeightImage from 'react-native-auto-height-image';
import fragrances from '../../../../../Assets/images/categories/fragrances.jpg';
import {categoriesImageMapData} from '../data';

const {width} = Dimensions.get('screen');

function CategoryItem({item}) {
  const navigation = useNavigation();

  const prettifyName = name => {
    return name.replace(/-([a-z])/g, function (match, group1) {
      return ' ' + group1.toUpperCase();
    });
  };

  const onCategoryItemPress = useCallback(() => {
    navigation.navigate('UserExternalRoutes', {
      screen: 'Catalog',
      params: {
        categoryName: item.name,
      },
    });
  }, [item.name, navigation]);

  return (
    <TouchableOpacity
      style={styles.root}
      activeOpacity={0.8}
      onPress={onCategoryItemPress}>
      <AutoHeightImage
        source={categoriesImageMapData[item.name] || fragrances}
        width={width * 0.9}
        borderRadius={10}
        style={styles.img}
      />

      <View style={styles.categoryBox}>
        <Text style={styles.categoryName}>{prettifyName(item.name)}</Text>
      </View>
    </TouchableOpacity>
  );
}

export default CategoryItem;

const styles = StyleSheet.create({
  root: {
    borderRadius: 10,
    marginBottom: 20,
    width: width * 0.93,
  },
  img: {
    width: '100%',
  },
  categoryBox: {
    position: 'absolute',
    bottom: 20,
    left: 15,
  },

  categoryName: {
    fontSize: 16,
    fontWeight: '600',
    fontFamily: Platform.OS === 'android' ? 'Gotham-Book' : 'Gotham',
    color: '#fff',
    textTransform: 'uppercase',
  },
});
