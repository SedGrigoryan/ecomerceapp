import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import {useSelector} from 'react-redux';
import {FlatGrid} from 'react-native-super-grid';
import ProductItem from '../../../../Components/ProductItem';
import SkeletonPlaceholders from '../../../../Components/SkeletonPlaceholder';

function Wishlist() {
  const {wishlistWithData} = useSelector(state => state.wishlistReducer);

  return (
    <View style={styles.root}>
      {wishlistWithData ? (
        wishlistWithData.length ? (
          <FlatGrid
            itemDimension={150}
            data={wishlistWithData}
            keyExtractor={item => item.title}
            renderItem={({item, index}) => (
              <ProductItem key={index} item={item} />
            )}
          />
        ) : (
          <View style={styles.emptyWishlist}>
            <Text style={styles.noWishList}>Wishlist is empty</Text>
          </View>
        )
      ) : (
        <FlatGrid
          data={['1', '2', '3', '4', '5', '6']}
          itemDimension={150}
          keyExtractor={item => item}
          renderItem={({item}) => (
            <SkeletonPlaceholders key={`skeleton-${item}`} type={'product'} />
          )}
        />
      )}
    </View>
  );
}

export default Wishlist;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
  },
  emptyWishlist: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  noWishList: {
    fontSize: 15,
    fontWeight: '400',
    fontFamily: 'Gotham',
    paddingRight: 15,
    color: '#1E1D1D',
  },
});
