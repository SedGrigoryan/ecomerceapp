import React from 'react';
import {StyleSheet, View, FlatList} from 'react-native';
import {FlatGrid} from 'react-native-super-grid';
import ProductItem from '../../../../../../Components/ProductItem';
import ProductSectionHeader from './ProductSectionHeader';

function ProductList({data}) {
  return (
    <FlatList
      data={data}
      keyExtractor={item => item.title}
      renderItem={({item, index}) => (
        <View style={styles.sectionContainer}>
          <ProductSectionHeader key={index} section={item} />
          <FlatGrid
            itemDimension={130}
            data={item.data}
            keyExtractor={product => product.title}
            renderItem={({item, index}) => (
              <ProductItem key={index} item={item} />
            )}
          />
        </View>
      )}
    />
  );
}

export default ProductList;

const styles = StyleSheet.create({
  gridView: {
    marginTop: 20,
    flex: 1,
  },
  sectionContainer: {
    borderBottomWidth: 1,
    borderColor: '#DFDFDF',
    marginHorizontal: 8,
    marginTop: 10,
  },
});
