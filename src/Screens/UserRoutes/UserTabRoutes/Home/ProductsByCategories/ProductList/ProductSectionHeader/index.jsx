import React, {useCallback} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {useNavigation} from '@react-navigation/native';

function ProductSectionHeader({section}) {
  const navigation = useNavigation();

  const onSeeAllButtonPress = useCallback(() => {
    navigation.navigate('UserExternalRoutes', {
      screen: 'Catalog',
      params: {
        categoryName: section.title,
      },
    });
  }, [navigation, section.title]);

  const prettifyName = name => {
    return name.replace(/-([a-z])/g, function (match, group1) {
      return ' ' + group1.toUpperCase();
    });
  };

  return (
    <View style={styles.titleContainer}>
      <Text style={styles.productCategoryName}>
        {prettifyName(section.title)}
      </Text>
      <TouchableOpacity
        style={styles.seeAllButton}
        onPress={onSeeAllButtonPress}>
        <Text style={styles.seeAll}>See all</Text>
      </TouchableOpacity>
    </View>
  );
}

export default ProductSectionHeader;

const styles = StyleSheet.create({
  productCategoryName: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Gotham-Medium',
    textTransform: 'uppercase',
    color: '#1E1D1D',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 15,
    marginBottom: 10,
  },

  seeAllButton: {
    borderBottomWidth: 1,
    borderBottomColor: '#7867BE',
  },

  seeAll: {
    fontSize: 12,
    fontWeight: '700',
    color: '#7867BE',
    textTransform: 'uppercase',
    fontFamily: 'Gotham-Ultra',
  },
});
