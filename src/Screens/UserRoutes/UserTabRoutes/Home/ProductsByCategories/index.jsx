import React, {useEffect} from 'react';
import {View, StyleSheet} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {FlatGrid} from 'react-native-super-grid';
import SkeletonPlaceholders from '../../../../../Components/SkeletonPlaceholder';
import ProductList from './ProductList';
import {getCategoriesAction} from '../../../../../Store/reducers/categoriesSlice';
import {getProductsByCategoryAction} from '../../../../../Store/reducers/productsSlice';

function ProductsByCategories() {
  const {categories} = useSelector(state => state?.categoryReducer);

  const {productsByCategory} = useSelector(state => state.productReducer);
  const dispatch = useDispatch();
  useEffect(() => {
    dispatch(getCategoriesAction());
  }, [dispatch]);

  useEffect(() => {
    if (categories?.length) {
      dispatch(getProductsByCategoryAction(categories));
    }
  }, [categories, dispatch]);

  return (
    <View style={styles.container}>
      {productsByCategory?.length ? (
        <ProductList data={productsByCategory} />
      ) : (
        <FlatGrid
          data={['1', '2', '3', '4', '5', '6']}
          itemDimension={150}
          keyExtractor={item => item}
          renderItem={({item}) => (
            <SkeletonPlaceholders key={`skeleton-${item}`} type={'product'} />
          )}
        />
      )}
    </View>
  );
}

export default ProductsByCategories;

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
});
