import React from 'react';
import {View, Dimensions, StyleSheet} from 'react-native';
import bannerOne from '../../../../../Assets/images/banner/banner1.jpg';
import bannerTwo from '../../../../../Assets/images/banner/banner2.jpg';
import bannerThree from '../../../../../Assets/images/banner/banner3.jpg';
import {SliderBox} from 'react-native-image-slider-box';

function HomeBanner() {
  const data = [bannerOne, bannerTwo, bannerThree];
  const {width} = Dimensions.get('window');

  return (
    <View style={styles.container}>
      <SliderBox
        dotColor="#000"
        inactiveDotColor="#fff"
        images={data}
        sliderBoxHeight={200}
        autoplay={true}
        autoplayInterval={5000}
        ImageComponentStyle={{
          borderRadius: 10,
          width: width * 0.93,
        }}
      />
    </View>
  );
}

export default HomeBanner;

const styles = StyleSheet.create({
  container: {
    marginTop: 10,
    flex: 1,
  },
});
