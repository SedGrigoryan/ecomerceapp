import React from 'react';
import {ScrollView, StyleSheet} from 'react-native';
import HomeBanner from './HomeBanner';
import ProductsByCategories from './ProductsByCategories';

function Home() {
  return (
    <ScrollView style={styles.root} contentContainerStyle={styles.content}>
      <HomeBanner />
      <ProductsByCategories />
    </ScrollView>
  );
}

export default Home;

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#fff',
    flex: 1,
    paddingVertical: 10,
  },
});
