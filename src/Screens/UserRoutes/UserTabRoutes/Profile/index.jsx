import React, {useCallback} from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';
import {useSelector, useDispatch} from 'react-redux';
import ProfileInfo from './ProfileInfo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import {revertAll} from '../../../../Store';
import {logOutAction} from '../../../../Store/reducers/authSlice';

function Profile() {
  const dispatch = useDispatch();
  const {authUser} = useSelector(state => state.authReducer);

  const onLogOutPress = useCallback(() => {
    dispatch(logOutAction());
    dispatch(revertAll());
  }, [dispatch]);

  return (
    <View style={styles.root}>
      <ProfileInfo profile={authUser} />
      <View style={styles.logOutBox}>
        <TouchableOpacity style={styles.box} onPress={onLogOutPress}>
          <View style={styles.rowBox}>
            <MaterialIcons name={'logout'} size={25} color={'#D1D1D1'} />
            <Text style={styles.logOutText}>Log Out</Text>
          </View>
          <MaterialIcons
            name={'arrow-forward-ios'}
            size={25}
            color={'#D9D9D9'}
          />
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Profile;

const styles = StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#fff',
    paddingHorizontal: 20,
  },
  box: {
    width: '100%',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    borderBottomWidth: 1,
    borderTopWidth: 1,
    borderColor: '#E6E6E6',
  },
  rowBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingVertical: 20,
  },
  logOutBox: {
    marginVertical: 10,
    flex: 1,
    paddingVertical: 15,
    alignItems: 'flex-end',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  logOutText: {
    fontSize: 14,
    color: '#1E1D1D',
    fontWeight: '400',
    fontFamily: 'Gotham-Light',
    paddingLeft: 10,
  },
});
