import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import AutoHeightImage from 'react-native-auto-height-image';
import SkeletonPlaceholders from '../../../../../Components/SkeletonPlaceholder';

function ProfileInfo({profile}) {
  return (
    <View style={styles.infoContainer}>
      {profile ? (
        profile?.image ? (
          <AutoHeightImage source={{uri: profile?.image}} width={60} />
        ) : null
      ) : (
        <SkeletonPlaceholders />
      )}
      <View style={styles.infoBox}>
        <Text style={styles.strongText}>
          {profile?.firstName} {profile?.lastName}
        </Text>
        <Text style={styles.lightText}>
          {profile?.gender?.slice(0, 1).toUpperCase() +
            profile?.gender?.slice(1)}
        </Text>
      </View>
    </View>
  );
}

export default ProfileInfo;

const styles = StyleSheet.create({
  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: '#E6E6E6',
    paddingBottom: 25,
    marginTop: 15,
  },
  infoBox: {
    marginLeft: 20,
  },
  strongText: {
    fontSize: 16,
    color: '#1E1D1D',
    lineHeight: 35,
    fontWeight: '400',
    fontFamily: 'Gotham-Medium',
  },
  lightText: {
    fontSize: 16,
    color: '#ADADAD',
    fontWeight: '300',
    fontFamily: 'Gotham-Light',
  },
});
