import React, {useEffect, useState} from 'react';
import {Text, View} from 'react-native';
import {NavigationContainer} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import AuthRoutes from './Auth';
import UserRoutes from './UserRoutes';
import SplashScreen from '../Components/SplashScreen';
import Storage from '../Helpers/asyncStorage';
import {getAuthUserAction} from '../Store/reducers/authSlice';
import {getWishListAction} from '../Store/reducers/wishlistSlice';
import auth from './Auth';

function AppNavigator() {
  const {authUser} = useSelector(state => state.authReducer);
  const [loading, setLoading] = useState(false);
  const dispatch = useDispatch();

  useEffect(() => {
    (async function getToken() {
      setLoading(true);
      const token = await Storage.getItem('authToken');
      if (token) {
        dispatch(getAuthUserAction(token));
        dispatch(getWishListAction());
      } else {
        setLoading(false);
      }
    })();
  }, [dispatch]);

  useEffect(() => {
    if (authUser) {
      setLoading(false);
    }
  }, [authUser]);

  if (loading) {
    return <SplashScreen />;
  }

  return (
    <NavigationContainer>
      {!authUser ? <AuthRoutes /> : <UserRoutes />}
    </NavigationContainer>
  );
}

export default AppNavigator;
