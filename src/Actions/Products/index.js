import axios from 'axios';
import {BASE_API_URL} from '../../Constants';

export const getProductDetails = async productId => {
  try {
    const response = await axios.get(`${BASE_API_URL}/products/${productId}`);
    if (response.status === 200 && response.data) {
      return response.data;
    }
  } catch (err) {
    return null;
  }
};

export const getProductsByCategory = async categoryName => {
  try {
    const response = await axios.get(
      `${BASE_API_URL}/products/category/${categoryName}`,
    );
    if (response.status === 200 && response.data?.products) {
      return response.data.products;
    }
  } catch (err) {
    return [];
  }
};
