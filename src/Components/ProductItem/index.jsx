import React, {useCallback} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  Dimensions,
  Platform,
} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import AutoHeightImage from 'react-native-auto-height-image';
import Ionicons from 'react-native-vector-icons/Ionicons';
import {addToWishList} from '../../Store/reducers/wishlistSlice';

const {width} = Dimensions.get('screen');

function ProductItem({item}) {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {wishlist} = useSelector(state => state.wishlistReducer);

  const onProductItemPress = useCallback(() => {
    navigation.navigate('UserExternalRoutes', {
      screen: 'ProductDetails',
      params: {
        productId: item.id,
      },
    });
  }, [item.id, navigation]);

  const onHeartIconPress = useCallback(() => {
    dispatch(addToWishList(item));
  }, [dispatch, item]);

  return (
    <TouchableOpacity
      style={styles.productContainer}
      opacity={0.9}
      onPress={onProductItemPress}>
      <View style={styles.imgContainer}>
        <AutoHeightImage
          source={{uri: item.thumbnail}}
          maxHeight={164}
          width= {width * 0.43}
          style={styles.img}
        />
        <TouchableOpacity style={styles.heart} onPress={onHeartIconPress}>
          {wishlist?.length && wishlist.includes(item.id) ? (
            <Ionicons name="heart" size={20} color="#F34040" />
          ) : (
            <Ionicons name="heart-outline" size={20} color="#F34040" />
          )}
        </TouchableOpacity>
      </View>
      <Text style={styles.productTitle}>{item.title}</Text>
      <View style={styles.infoContainer}>
        <View style={styles.ratingBox}>
          <Ionicons name="star" size={13} color="#FFC700" />
          <Text style={styles.rating}>{item.rating}</Text>
        </View>
        <Text style={styles.price}>${item.price}</Text>
      </View>
    </TouchableOpacity>
  );
}

export default ProductItem;

const styles = StyleSheet.create({
  productContainer: {
    borderRadius: 10,
    marginBottom: 25,
    height:220,
  },

  productCategoryName: {
    fontSize: 16,
    fontWeight: '500',
    fontFamily: 'Gotham-medium',
    textTransform: 'uppercase',
  },
  titleContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    marginTop: 15,
    marginBottom: 10,
  },
  productTitle: {
    fontSize: 11,
    color: '#1E1D1D',
    textTransform: 'uppercase',
    fontFamily: 'Gotham-Book',
    marginTop: 10,
    marginBottom: 8,
    paddingLeft: 3,
    lineHeight: 20,
  },

  seeAllButton: {
    borderBottomWidth: 1,
    borderBottomColor: '#7867BE',
  },

  seeAll: {
    fontSize: 12,
    fontWeight: '700',
    color: '#7867BE',
    textTransform: 'uppercase',
  },
  productThumbNail: {
    borderRadius: 10,
  },
  imgContainer: {
    height: 164,
    maxHeight: 164,
  },
  img: {
    borderRadius: 10,
    height: '100%',
  },

  heart: {
    position: 'absolute',
    top: 7,
    right: 15,
  },

  infoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },

  ratingBox: {
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: 5,
  },

  rating: {
    fontSize: 10,
    color: '#1E1D1D',
    marginLeft: 3,
    fontFamily: 'Gotham-Book',
  },

  price: {
    fontSize: 12,
    color: '#1E1D1D',
    fontWeight: '500',
    fontFamily: 'Gotham-Book',
  },
});
