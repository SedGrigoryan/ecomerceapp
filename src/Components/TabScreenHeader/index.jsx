import React, {useCallback} from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';

function TabScreenHeader({routeName, img, leftIcon, rightIcon}) {
  const navigation = useNavigation();

  const onBackButtonClick = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const onSearchButtonClick = useCallback(() => {
    navigation.navigate('UserExternalRoutes', {
      screen: 'Search',
    });
  }, [navigation]);

  return (
    <View style={styles.root}>
      {img ? (
        <Image
          source={require('../../Assets/images/logo/logo.png')}
          style={styles.logo}
        />
      ) : null}

      {leftIcon?.length ? (
        <TouchableOpacity onPress={onBackButtonClick}>
          <Ionicons name={leftIcon} color={'#343434'} size={25} />
        </TouchableOpacity>
      ) : (
        <View />
      )}
      <Text style={styles.routeName}>{routeName}</Text>

      {rightIcon?.length ? (
        <TouchableOpacity onPress={onSearchButtonClick}>
          <Ionicons name={rightIcon} color="#343434" size={24} />
        </TouchableOpacity>
      ) : (
        <View style={styles.emptyView} />
      )}
    </View>
  );
}

export default TabScreenHeader;

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: '#fff',
  },
  logo: {
    width: 43,
    height: 26,
  },
  routeName: {
    fontSize: 15,
    fontWeight: '600',
    fontFamily: 'Gotham-Book',
    color: '#1E1D1D',

    textTransform: 'uppercase',
  },
  emptyView: {
    paddingVertical: 12,
  },
});
