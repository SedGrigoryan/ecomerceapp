import React, {useCallback} from 'react';
import {View, Text, TouchableOpacity, StyleSheet, Platform} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import {useNavigation} from '@react-navigation/native';
import Ionicons from 'react-native-vector-icons/Ionicons';
import SearchInput from '../../Screens/UserRoutes/UserExternalRoutes/Search/SearchInput';
import {addToWishList} from '../../Store/reducers/wishlistSlice';

function StackScreenHeader({
  routeName,
  search,
  leftIcon,
  rightIcon,
  rightIconAction,
  product,
}) {
  const navigation = useNavigation();
  const dispatch = useDispatch();
  const {wishlist} = useSelector(state => state.wishlistReducer);

  const onBackButtonClick = useCallback(() => {
    navigation.goBack();
  }, [navigation]);

  const onRightIconClick = useCallback(() => {
    if (rightIconAction === 'favorite' && product?.id) {
      dispatch(addToWishList(product));
    } else {
      navigation.navigate('UserExternalRoutes', {
        screen: 'Search',
      });
    }
  }, [dispatch, navigation, product, rightIconAction]);

  return (
    <View style={styles.root}>
      {leftIcon?.length ? (
        <TouchableOpacity onPress={onBackButtonClick}>
          <Ionicons name={leftIcon} color={'#343434'} size={25} />
        </TouchableOpacity>
      ) : (
        <View />
      )}
      {search ? <SearchInput /> : null}
      {routeName ? <Text style={styles.routeName}>{routeName}</Text> : null}

      {rightIcon?.length ? (
        <TouchableOpacity onPress={onRightIconClick}>
          {rightIconAction === 'favorite' && wishlist.includes(product?.id) ? (
            <Ionicons name={'heart'} color="#F34040" size={24} />
          ) : (
            <Ionicons name={rightIcon} color="#343434" size={24} />
          )}
        </TouchableOpacity>
      ) : (
        <View style={styles.emptyView} />
      )}
    </View>
  );
}

export default StackScreenHeader;

const styles = StyleSheet.create({
  root: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    padding: 15,
    backgroundColor: '#fff',
  },
  logo: {
    width: 43,
    height: 26,
  },
  routeName: {
    fontSize: 15,
    fontWeight: '600',
    fontFamily: Platform?.OS === 'ios' ? 'Gotham' : 'Gotham-Book',
    textTransform: 'uppercase',
    color: '#1E1D1D',
  },
  emptyView: {
    paddingVertical: 12,
  },
});
