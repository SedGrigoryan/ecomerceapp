import React, {useEffect, useRef} from 'react';
import {View, StyleSheet} from 'react-native';
import LottieView from 'lottie-react-native';

export default function SplashScreen() {
  const animationRef = useRef();

  useEffect(() => {
    animationRef.current?.play();
  }, [animationRef]);

  return (
    <View style={styles.root}>
      <LottieView
        ref={animationRef}
        source={require('../../Assets/images/animation.json')}
        style={styles.view}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  view: {
    width: 150,
    height: 200,
  },
});
