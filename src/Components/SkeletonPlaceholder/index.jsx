import React from 'react';
import {Dimensions, StyleSheet} from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const ProductSkeleton = ({itemWidth = 0.45, itemHeight = 215}) => {
  const {width} = Dimensions.get('screen');
  return (
    <SkeletonPlaceholder borderRadius={8}>
      <SkeletonPlaceholder.Item
        width={width * itemWidth}
        height={itemHeight}
        style={styles.productSkeleton}
      />
    </SkeletonPlaceholder>
  );
};

function SkeletonPlaceholders({type, width, height}) {
  const SkeletonComponentsMap = {
    product: ProductSkeleton,
  };

  const SkeletonComponent = SkeletonComponentsMap[type] || null;
  return (
    <>
      {SkeletonComponent ? (
        <SkeletonComponent itemWidth={width} itemHeight={height} />
      ) : (
        <SkeletonPlaceholder borderRadius={4}>
          <SkeletonPlaceholder.Item flexDirection="row" alignItems="center">
            <SkeletonPlaceholder.Item
              width={60}
              height={60}
              borderRadius={50}
            />
            <SkeletonPlaceholder.Item marginLeft={20}>
              <SkeletonPlaceholder.Item width={120} height={20} />
              <SkeletonPlaceholder.Item marginTop={6} width={80} height={20} />
            </SkeletonPlaceholder.Item>
          </SkeletonPlaceholder.Item>
        </SkeletonPlaceholder>
      )}
    </>
  );
}

export default SkeletonPlaceholders;

const styles = StyleSheet.create({
  productSkeleton: {
    marginBottom: 10,
  },
});
