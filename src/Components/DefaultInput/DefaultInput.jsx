import React from 'react';
import {Text, View, TextInput, StyleSheet} from 'react-native';

function DefaultInput({
  label,
  formikProps,
  name,
  style,
  inputStyles,
  edit,
  eye,
  editable = true,
  ...rest
}) {
  return (
    <>
      <View style={styles.root}>
        <Text style={styles.label}>
          {' '}
          {label} <Text style={styles.requiredIcon}>*</Text>
        </Text>
        <View style={styles.textInputContainer}>
          <TextInput
            onChangeText={formikProps.handleChange(name)}
            onBlur={formikProps.handleBlur(name)}
            value={`${formikProps.values[name]}`}
            defaultValue={`${formikProps.values[name]}`}
            editable={editable}
            autoCapitalize="none"
            style={{
              ...styles.textInput,
              ...inputStyles,
            }}
            {...rest}
          />
        </View>

        {formikProps.touched[name] && formikProps.errors[name] && (
          <Text style={styles.error}>{formikProps.errors[name]}</Text>
        )}
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  root: {
    marginBottom: 25,
    minWidth: '100%',
    width: '100%',
  },
  label: {
    fontSize: 16,
    fontFamily: 'Gotham-Book',
    color: '#1E1D1D',
  },
  requiredIcon: {
    fontSize: 14,
    color: 'red',
    fontFamily: 'Gotham-Black',
  },

  textInputContainer: {
    marginHorizontal: 5,
    marginTop: 10,
    height: 52,
    borderWidth: 1,
    borderColor: '#E6E6E6',
    borderRadius: 10,
    paddingLeft: 15,
  },

  textInput: {
    flex: 1,
    height: 52,
    color: '#1E1D1D',
  },
  error: {
    color: '#ee2864',
    fontSize: 12,
    paddingLeft: 15,
    paddingTop: 2,
    fontFamily: 'Gotham-Black',
  },
});

export default React.memo(DefaultInput);
