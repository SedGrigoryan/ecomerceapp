import {configureStore, combineReducers, createAction} from '@reduxjs/toolkit';
import categoriesSlice from './reducers/categoriesSlice';
import productsSlice from './reducers/productsSlice';
import wishlistSlice from './reducers/wishlistSlice';
import authSlice from './reducers/authSlice';
import searchSlice from './reducers/searchSlice';

const rootReducer = combineReducers({
  categoryReducer: categoriesSlice,
  productReducer: productsSlice,
  wishlistReducer: wishlistSlice,
  authReducer: authSlice,
  searchReducer: searchSlice,
});

export const revertAll = createAction('REVERT_ALL');

const store = configureStore({
  reducer: rootReducer,
  middleware: getDefaultMiddleware =>
    getDefaultMiddleware({
      serializableCheck: false,
    }),
});
export default store;
