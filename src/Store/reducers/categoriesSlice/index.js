import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {BASE_API_URL} from '../../../Constants/index';

export const getCategoriesAction = createAsyncThunk(
  'getCategoriesAction',
  async function (body, {rejectWithValue}) {
    try {
      const response = await axios.get(`${BASE_API_URL}/products/categories`);
      if (response.status === 200) {
        return response.data;
      }
    } catch (err) {
      return rejectWithValue(err.response);
    }
  },
);
const initialState = {
  categories: [],
  error: null,
};
const categoriesSlice = createSlice({
  name: 'CATEGORIES',
  initialState,
  reducers: {},
  extraReducers: builder => {
    builder.addCase(getCategoriesAction.fulfilled, (state, action) => {
      state.error = null;
      state.categories = action.payload;
    });
    builder.addCase(getCategoriesAction.rejected, (state, action) => {
      state.error =
        action.paylaod || ' Something went wrong. Please try again.';
      state.categories = [];
    });
    builder.addCase('REVERT_ALL', () => initialState);
  },
});

export default categoriesSlice.reducer;
