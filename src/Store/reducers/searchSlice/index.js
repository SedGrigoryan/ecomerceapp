import {createAction, createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {BASE_API_URL} from '../../../Constants';

export const initialSearchAction = createAsyncThunk(
  'initialSearchAction',
  async function (value, {rejectWithValue}) {
    try {
      const response = await axios.get(
        `${BASE_API_URL}/products/search?q=${value}&skip=0&limit=10`,
      );
      if (response.status === 200) {
        return response.data;
      }
    } catch (err) {
      rejectWithValue(err.response);
    }
  },
);

export const searchAction = createAsyncThunk(
  'searchAction',
  async function (body, {rejectWithValue}) {
    try {
      const response = await axios.get(
        `${BASE_API_URL}/products/search?q=${body.value}&skip=${body.skip}&limit=${body.limit}`,
      );
      if (response.status === 200) {
        return response.data;
      }
    } catch (err) {
      rejectWithValue(err.response);
    }
  },
);
const initialState = {
  searchResult: [],
  skip: 0,
  limit: 10,
  total: 0,
  loading: false,
  searchText: '',
};

export const clearAction = createAction('CLEAR_STATE');
const searchSlice = createSlice({
  name: 'SEARCH',
  initialState,
  reducers: {
    setSearchText: (state, action) => {
      state.searchText = action.payload;
      state.skip = 0;
      state.total = 0;
      state.searchResult = [];
    },
  },
  extraReducers: builder => {
    builder.addCase(initialSearchAction.fulfilled, (state, action) => {
      if (state.searchText?.length) {
        state.searchResult = action.payload.products;
        state.skip = action.payload.skip + state.limit;
        state.total = action.payload.total;
      }
    });
    builder.addCase(searchAction.fulfilled, (state, action) => {
      if (state.searchText?.length) {
        state.searchResult = [
          ...state.searchResult,
          ...action.payload.products,
        ];
        state.skip = action.payload.skip + state.limit;
        state.total = action.payload.total;
        state.loading = false;
      }
    });
    builder.addCase(searchAction.pending, (state, action) => {
      state.loading = true;
    });
    builder.addCase('REVERT_ALL', () => initialState);
    builder.addCase('CLEAR_STATE', () => initialState);
  },
});

export const {setSearchText} = searchSlice.actions;

export default searchSlice.reducer;
