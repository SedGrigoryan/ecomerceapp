import {createSlice, createAsyncThunk} from '@reduxjs/toolkit';
import Storage from '../../../Helpers/asyncStorage';

const initialState = {
  wishlist: [],
  wishlistWithData: [],
};

export const getWishListAction = createAsyncThunk(
  'getWishListAction',
  async function (_, {rejectWithValue}) {
    try {
      const wishlistStorageData = await Storage.getItem('userWishList');
      return wishlistStorageData;
    } catch (err) {
      return rejectWithValue(err.response);
    }
  },
);

const wishlistSlice = createSlice({
  name: 'WISHLIST',
  initialState,
  reducers: {
    addToWishList: (state, action) => {
      let currentWishList = [...state.wishlist];
      let currentWishListWithData = [...state.wishlistWithData];
      if (currentWishList.includes(action.payload.id)) {
        currentWishList = currentWishList.filter(
          item => item !== action.payload.id,
        );
        currentWishListWithData = currentWishListWithData.filter(
          item => item.id !== action.payload.id,
        );
      } else {
        currentWishList.push(action.payload.id);
        currentWishListWithData.push(action.payload);
      }

      state.wishlist = currentWishList;
      state.wishlistWithData = currentWishListWithData;

      Storage.setItem('userWishList', currentWishListWithData);
    },
  },
  extraReducers: builder => {
    builder.addCase(getWishListAction.fulfilled, (state, action) => {
      state.wishlistWithData = action.payload;
      state.wishlist = action.payload.map(item => item.id);
    });
    builder.addCase('REVERT_ALL', () => initialState);
  },
});

export const {addToWishList} = wishlistSlice.actions;
export default wishlistSlice.reducer;
