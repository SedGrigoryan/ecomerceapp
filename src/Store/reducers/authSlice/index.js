import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {BASE_API_URL} from '../../../Constants';
import Storage from '../../../Helpers/asyncStorage';

export const loginAction = createAsyncThunk(
  'loginAction',
  async function (body, {rejectWithValue}) {
    try {
      const response = await axios.post(`${BASE_API_URL}/auth/login`, body);
      if (response.status === 200) {
        Storage.setItem('authToken', response.data.token);
        return response.data;
      }
    } catch (err) {
      return rejectWithValue(err?.response?.data?.message);
    }
  },
);

export const getAuthUserAction = createAsyncThunk(
  'getAuthUserAction',
  async function (token, {rejectWithValue}) {
    try {
      const response = await axios.get(`${BASE_API_URL}/auth/me`, {
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      if (response.status === 200) {
        return response.data;
      }
    } catch (err) {
      return rejectWithValue(err?.response?.data?.message);
    }
  },
);

export const logOutAction = createAsyncThunk(
  'logOutAction',
  async function (_, {rejectWithValue}) {
    try {
      await Storage.removeItem('authToken');
      await Storage.removeItem('userWishList');
    } catch (err) {
      rejectWithValue(err.response);
    }
  },
);
const initialState = {
  authUser: null,
  loginError: null,
  loading: false,
};
const authSlice = createSlice({
  name: 'AUTH',
  initialState,
  extraReducers: builder => {
    builder.addCase(loginAction.fulfilled, (state, action) => {
      state.authUser = action.payload;
      state.loginError = null;
      state.loading = false;
    });
    builder.addCase(loginAction.rejected, (state, action) => {
      state.authUser = null;
      state.loginError = action.payload || 'Something went wrong';
      state.loading = false;
    });
    builder.addCase(loginAction.pending, state => {
      state.loading = true;
    });
    builder.addCase(getAuthUserAction.fulfilled, (state, action) => {
      state.authUser = action.payload;
    });
    builder.addCase(logOutAction.fulfilled, state => {
      state.authUser = null;
    });
    builder.addCase('REVERT_ALL', () => initialState);
  },
});

export default authSlice.reducer;
