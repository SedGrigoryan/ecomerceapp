import {createAsyncThunk, createSlice} from '@reduxjs/toolkit';
import axios from 'axios';
import {BASE_API_URL} from '../../../Constants';

const getProducstByCategory = async category => {
  try {
    const response = await axios.get(
      `${BASE_API_URL}/products/category/${category}?skip=0&limit=4`,
    );
    if (response.status === 200 && response?.data?.products) {
      return response.data?.products;
    }
  } catch (err) {
    return [];
  }
};

export const getProductsByCategoryAction = createAsyncThunk(
  'getProductsByCategoryAction',
  async function (categories, {rejectWithValue}) {
    try {
      const productsByCategories = [];
      await Promise.all(
        categories.map(async category => {
          const products = await getProducstByCategory(category);
          productsByCategories.push({
            title: category,
            data: products,
          });
        }),
      );
      return productsByCategories;
    } catch (err) {
      return rejectWithValue(err.response);
    }
  },
);
const initialState = {
  productsByCategory: [],
  regError: null,
};
const productsSlice = createSlice({
  name: 'PRODUCTS',
  initialState,
  extraReducers: builder => {
    builder.addCase(getProductsByCategoryAction.fulfilled, (state, action) => {
      state.productsByCategory = action.payload;
      state.regError = null;
    });
    builder.addCase(getProductsByCategoryAction.rejected, (state, action) => {
      state.productsByCategory = [];
      state.regError = action.payload || 'Something went wrong';
    });
    builder.addCase('REVERT_ALL', () => initialState);
  },
});

export default productsSlice.reducer;
