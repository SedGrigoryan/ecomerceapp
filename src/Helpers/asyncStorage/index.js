import AsyncStorage from '@react-native-async-storage/async-storage';

const Storage = {
  getItem: async key => {
    try {
      const data = await AsyncStorage.getItem(key);
      return JSON.parse(data);
    } catch (err) {
      return null;
    }
  },

  setItem: async (key, data) => {
    try {
      await AsyncStorage.setItem(key, JSON.stringify(data));
    } catch (err) {}
  },

  removeItem: async key => {
    try {
      await AsyncStorage.removeItem(key);
    } catch (err) {}
  },
};

export default Storage;
